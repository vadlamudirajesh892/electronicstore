package com.lcwd.electronic.store.ElectronicStore.exception;

public class ResourceNotFoundExpeption extends RuntimeException {

	public ResourceNotFoundExpeption() {
	super("Resource not found ");
	}

	public ResourceNotFoundExpeption(String msg) {
		super(msg);
	}
}
