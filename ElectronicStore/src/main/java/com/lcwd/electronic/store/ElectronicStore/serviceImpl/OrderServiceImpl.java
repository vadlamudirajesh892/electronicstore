package com.lcwd.electronic.store.ElectronicStore.serviceImpl;

import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_EXCEPRION_RESOURCE_CREATE_FIND_UID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_EXCEPTION_BEDAPI_RESPONSE_CREATE_CART_SIZE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_EXCEPTION_RESOURCE_CREATE_FIND_CARTID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_EXCEPTION_RESOURCE_DELETE_FIND_UID;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.lcwd.electronic.store.ElectronicStore.dtos.CreateOrderRequest;
import com.lcwd.electronic.store.ElectronicStore.dtos.OrderDto;
import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.entities.Cart;
import com.lcwd.electronic.store.ElectronicStore.entities.CartItem;
import com.lcwd.electronic.store.ElectronicStore.entities.Order;
import com.lcwd.electronic.store.ElectronicStore.entities.OrderItem;
import com.lcwd.electronic.store.ElectronicStore.entities.User;
import com.lcwd.electronic.store.ElectronicStore.exception.BadApiResponseException;
import com.lcwd.electronic.store.ElectronicStore.exception.ResourceNotFoundExpeption;
import com.lcwd.electronic.store.ElectronicStore.helper.Helper;
import com.lcwd.electronic.store.ElectronicStore.repositories.CartRepository;
import com.lcwd.electronic.store.ElectronicStore.repositories.OrderRepository;
import com.lcwd.electronic.store.ElectronicStore.repositories.UserRepository;
import com.lcwd.electronic.store.ElectronicStore.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private CartRepository cartRepository;

	@Override
	public OrderDto createOrder(CreateOrderRequest orderDto) {

		String userId = orderDto.getUserId();
		String cartId = orderDto.getCartId();
		// fetch user
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(ORDER_EXCEPRION_RESOURCE_CREATE_FIND_UID));

		// fetch cart
		Cart cart = cartRepository.findById(cartId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(ORDER_EXCEPTION_RESOURCE_CREATE_FIND_CARTID));

		List<CartItem> cartItems = cart.getItems();

		if (cartItems.size() <= 0) {
			throw new BadApiResponseException(ORDER_EXCEPTION_BEDAPI_RESPONSE_CREATE_CART_SIZE);

		}

		// other checks
		/*
		 * Order order = Order.builder() .billingName(orderDto.getBillingName())
		 * .billingPhone(orderDto.getBillingPhone())
		 * .billingAddress(orderDto.getBillingAddress()) .orderedDate(new Date())
		 * .deliveredDate(null) .paymentStatus(orderDto.getPaymentStatus())
		 * .orderStatus(orderDto.getOrderStatus())
		 * .orderId(UUID.randomUUID().toString()) .user(user) .build();
		 */
		Order order = null;

		Helper.dtoToEntity(orderDto);
		order.setUser(user);

//        orderItems,amount

		AtomicReference<Integer> orderAmount = new AtomicReference<>(0);
		List<OrderItem> orderItems = cartItems.stream().map(cartItem -> {
			/*
			 * OrderItem orderItem = OrderItem.builder() .quantity(cartItem.getQuantity())
			 * .product(cartItem.getProduct()) .totalPrice(cartItem.getQuantity() *
			 * cartItem.getProduct().getDiscountedPrice()) .order(order) .build();
			 * 
			 */
//            CartItem->OrderItem
			OrderItem orderIteam = new OrderItem();
			orderIteam.setQuantity(cartItem.getQuantity());
			orderIteam.setProduct(cartItem.getProduct());
			orderIteam.setTotalPrice(cartItem.getQuantity() * cartItem.getProduct().getDiscountedPrice());
			orderIteam.setOrder(order);
			orderAmount.set(orderAmount.get() + orderIteam.getTotalPrice());
		
		 return orderIteam;
		}).collect(Collectors.toList());

		order.setOrderItems(orderItems);
		order.setOrderAmount(orderAmount.get());

		//
		cart.getItems().clear();
		cartRepository.save(cart);
		Order savedOrder = orderRepository.save(order);
		return modelMapper.map(savedOrder, OrderDto.class);
	}

	@Override
	public void removeOrder(String orderId) {

		Order order = orderRepository.findById(orderId)
				.orElseThrow(() -> new ResourceNotFoundExpeption("order is not found !!"));
		orderRepository.delete(order);

	}

	@Override
	public List<OrderDto> getOrdersOfUser(String userId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(ORDER_EXCEPTION_RESOURCE_DELETE_FIND_UID));
		List<Order> orders = orderRepository.findByUser(user);
		List<OrderDto> orderDtos = orders.stream().map(order -> modelMapper.map(order, OrderDto.class))
				.collect(Collectors.toList());
		return orderDtos;
	}

	@Override
	public PageableResponse<OrderDto> getOrders(int pageNumber, int pageSize, String sortBy, String sortDir) {
		Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
		Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
		Page<Order> page = orderRepository.findAll(pageable);
		return Helper.getPageableResponse(page, OrderDto.class);
	}
}
