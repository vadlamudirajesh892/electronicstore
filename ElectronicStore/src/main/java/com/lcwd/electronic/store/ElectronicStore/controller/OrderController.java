package com.lcwd.electronic.store.ElectronicStore.controller;

import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_ALL;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_BY_UID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_CONTROLLER_REQUEST_MAPPING;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_DELETE_BY_OID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ORDER_SAVE;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lcwd.electronic.store.ElectronicStore.dtos.ApiResponseMessage;
import com.lcwd.electronic.store.ElectronicStore.dtos.CreateOrderRequest;
import com.lcwd.electronic.store.ElectronicStore.dtos.OrderDto;
import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.helper.Helper;
import com.lcwd.electronic.store.ElectronicStore.service.OrderService;

@RestController
@RequestMapping(ORDER_CONTROLLER_REQUEST_MAPPING)
public class OrderController {

	@Autowired
	private OrderService orderService;

	private ApiResponseMessage response = Helper.getInstance();

	// create
	@PostMapping(ORDER_SAVE)
	public ResponseEntity<OrderDto> createOrder(@Valid @RequestBody CreateOrderRequest request) {
		OrderDto order = orderService.createOrder(request);
		return new ResponseEntity<>(order, HttpStatus.CREATED);
	}

	// @PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(ORDER_DELETE_BY_OID)
	public ResponseEntity<ApiResponseMessage> removeOrder(@PathVariable String orderId) {
		orderService.removeOrder(orderId);
		/*
		 * ApiResponseMessage responseMessage = ApiResponseMessage.builder()
		 * .status(HttpStatus.OK) .message("order is removed !!") .success(true)
		 * .build();
		 */

		response.setMessage("order is removed !!");
		response.setHttpStatus(HttpStatus.OK);
		response.setSuccess(true);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	// get orders of the user

	@GetMapping(ORDER_BY_UID)
	public ResponseEntity<List<OrderDto>> getOrdersOfUser(@PathVariable String userId) {
		List<OrderDto> ordersOfUser = orderService.getOrdersOfUser(userId);
		return new ResponseEntity<>(ordersOfUser, HttpStatus.OK);
	}

	@GetMapping(ORDER_ALL)
	public ResponseEntity<PageableResponse<OrderDto>> getOrders(
			@RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
			@RequestParam(value = "sortBy", defaultValue = "orderedDate", required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = "desc", required = false) String sortDir) {
		PageableResponse<OrderDto> orders = orderService.getOrders(pageNumber, pageSize, sortBy, sortDir);
		return new ResponseEntity<>(orders, HttpStatus.OK);
	}

}
