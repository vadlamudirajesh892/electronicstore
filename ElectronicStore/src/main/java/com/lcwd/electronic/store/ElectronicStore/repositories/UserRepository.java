package com.lcwd.electronic.store.ElectronicStore.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lcwd.electronic.store.ElectronicStore.entities.User;
@Repository
public interface UserRepository extends JpaRepository<User, String> {

	
	Optional<User> findByEmail(String email);
	Optional<User> findByEmailAndName(String email,String name);
	List<User> findByNameContaining(String keyword);
}
