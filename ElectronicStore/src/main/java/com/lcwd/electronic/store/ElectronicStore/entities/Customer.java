package com.lcwd.electronic.store.ElectronicStore.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@SuppressWarnings("serial")
//@Table(name = "customer_data", indexes = { @Index(name = "customer_data_index", columnList = "source") })
@Table(name = "customer_data")
@Entity
@Data
public class Customer {

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "user_data_id_seq", sequenceName = "user_data_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO,generator = "user_data_id_seq")
	private Long id;
	
	@Column(name = "name",length = 50)
	private String name;

}
