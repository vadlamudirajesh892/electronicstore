package com.lcwd.electronic.store.ElectronicStore.serviceImpl;

import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.USER_EXCEPTION_RESOURCE_GET;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.USER_EXCEPTION_RESOURCE_GET_EMAIL;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.USER_EXCEPTION_RUNTIME_DELETE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.USER_EXCEPTION_RUNTIME_UPDATE;
import static com.lcwd.electronic.store.ElectronicStore.constants.LoggerConstants.USER_IMAGE_NOT_FOUND;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.dtos.UserDto;
import com.lcwd.electronic.store.ElectronicStore.entities.User;
import com.lcwd.electronic.store.ElectronicStore.exception.ResourceNotFoundExpeption;
import com.lcwd.electronic.store.ElectronicStore.helper.Helper;
import com.lcwd.electronic.store.ElectronicStore.repositories.UserRepository;
import com.lcwd.electronic.store.ElectronicStore.service.UserService;

@Service
public class userServiceImpl implements UserService {
	private static User user1;

	@Value("${user.profile.image.path}")
	private String imagePath;

	private Logger log = LoggerFactory.getLogger(userServiceImpl.class);
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper mapper;

	@Autowired(required = false)
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public UserDto createUser(UserDto userDto) {
		String userId = UUID.randomUUID().toString();
		userDto.setUserId(userId);
		String a = passwordEncoder.encode(userDto.getPassword());

		User u = dtoToEntity(userDto);
		u.setPassword(a);
		User newuser = userRepository.save(u);
		UserDto createdDto = entityToDto(newuser);
		return createdDto;
	}

	@Override
	public UserDto updateUser(UserDto userDto, String userId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new RuntimeException(USER_EXCEPTION_RUNTIME_UPDATE));
		getInstance().setUserId(user.getUserId());
		getInstance().setName(user.getName());
		getInstance().setPassword(user.getPassword());
		getInstance().setEmail(user.getEmail());
		getInstance().setGender(user.getGender());
		getInstance().setAbout(user.getAbout());
		User updatedUser = userRepository.save(user1);
		UserDto updatedDto = entityToDto(updatedUser);
		return updatedDto;
	}

	@Override
	public void deleteUser(String userId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new RuntimeException(USER_EXCEPTION_RUNTIME_DELETE));
		String fullPath = imagePath + user.getImageName();
		try {
			Path path = Paths.get(fullPath);
			Files.delete(path);
		} catch (IOException e) {
			log.info(USER_IMAGE_NOT_FOUND, fullPath);
			e.printStackTrace();
		}
		userRepository.delete(user);
	}

	@Override
	public PageableResponse<UserDto> getAllUser(int pageNumber, int pageSize, String sortBy, String sortDir) {
		// Sort sort = Sort.by(sortBy);
		Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
		// page start from zero so changed to 1.
		Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, sort);
		Page<User> page = userRepository.findAll(pageable);

		/*
		 * List<User> users = page.getContent(); List<UserDto> dtoList =
		 * users.stream().map(user -> entityToDto(user)).collect(Collectors.toList());
		 * 
		 * PageableResponse<UserDto> response = new PageableResponse();
		 * response.setContent(dtoList); response.setPageNumber(page.getNumber());
		 * response.setPageSize(page.getSize());
		 * response.setTotalElements(page.getTotalElements());
		 * response.setTotalPages(page.getTotalPages());
		 * response.setLastPage(page.isLast()); return response;
		 */

		PageableResponse<UserDto> re = Helper.getPageableResponse(page, UserDto.class);
		return re;
	}

	@Override
	public UserDto getUserById(String userId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(USER_EXCEPTION_RESOURCE_GET));
		return entityToDto(user);
	}

	@Override
	public UserDto getUserByEmail(String email) {
		User user = userRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFoundExpeption(USER_EXCEPTION_RESOURCE_GET_EMAIL));
		return entityToDto(user);
	}

	@Override
	public List<UserDto> searchUser(String keyword) {
		List<User> users = userRepository.findByNameContaining(keyword);
		List<UserDto> dtoList = users.stream().map(user -> entityToDto(user)).collect(Collectors.toList());
		return dtoList;
	}

	private User dtoToEntity(UserDto userDto) {
		/*
		 * User u = new User(); u.setUserId(userDto.getUserId());
		 * u.setName(userDto.getName()); u.setPassword(userDto.getPassword());
		 * u.setEmail(userDto.getEmail()); u.setGender(userDto.getGender());
		 * u.setAbout(userDto.getAbout()); return u;
		 */
		return mapper.map(userDto, User.class);
	}

	private UserDto entityToDto(User user) {
		/*
		 * UserDto dto = new UserDto(); dto.setUserId(user.getUserId());
		 * dto.setName(user.getName()); dto.setPassword(user.getPassword());
		 * dto.setEmail(user.getEmail()); dto.setGender(user.getGender());
		 * dto.setAbout(user.getAbout()); return dto;
		 */
		return mapper.map(user, UserDto.class);
	}

	private static User getInstance() {
		if (user1 == null) {
			user1 = new User();
		}
		return user1;
	}
}
