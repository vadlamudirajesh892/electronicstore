package com.lcwd.electronic.store.ElectronicStore.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.lcwd.electronic.store.ElectronicStore.validate.ImageNameValid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
//@Min,@Max
	private String userId;
	@Size(min = 5, max = 15, message = "invalid name")
	private String name;
	@NotBlank(message = "Password is Required")
	private String password;
	@Email(message = "Invalid email Id")
	private String email;
	@Size(min = 4, max = 6)
	private String gender;
	@NotBlank(message = "write something about your self")
	private String about;
	// @Pattern annotation used for custom validation
	// custom validation annotation
	@ImageNameValid
	private String imageName;

}