package com.lcwd.electronic.store.ElectronicStore.controller;

import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_CONTROLLER_REQUEST_MAPPING;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_DELETE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_GET_ALL;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_GET_ID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_GET_PRODUCT_BY_CID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_PRODUCT_CREATE_BY_CID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_PRODUCT_UPDATE_BY_AID;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_SAVE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.CATEGORY_UPDATE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lcwd.electronic.store.ElectronicStore.dtos.ApiResponseMessage;
import com.lcwd.electronic.store.ElectronicStore.dtos.CategoryDto;
import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.dtos.ProductDto;
import com.lcwd.electronic.store.ElectronicStore.service.CategoryService;
import com.lcwd.electronic.store.ElectronicStore.service.ProductService;

@RestController
@RequestMapping(CATEGORY_CONTROLLER_REQUEST_MAPPING)
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ProductService productService;

	// create
	// @PreAuthorize("hasRole('ADMIN')")
	@PostMapping(CATEGORY_SAVE)
	public ResponseEntity<CategoryDto> createCategory(@Valid @RequestBody CategoryDto categoryDto) {
		// call service to save object
		CategoryDto categoryDto1 = categoryService.create(categoryDto);
		return new ResponseEntity<>(categoryDto1, HttpStatus.CREATED);
	}

	// update
	// @PreAuthorize("hasRole('ADMIN')")
	@PutMapping(CATEGORY_UPDATE)
	public ResponseEntity<CategoryDto> updateCategory(@PathVariable String categoryId,
			@RequestBody CategoryDto categoryDto) {
		CategoryDto updatedCategory = categoryService.update(categoryDto, categoryId);
		return new ResponseEntity<>(updatedCategory, HttpStatus.OK);
	}

	// delete
	// @PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(CATEGORY_DELETE)
	public ResponseEntity<ApiResponseMessage> deleteCategory(@PathVariable String categoryId) {
		categoryService.delete(categoryId);
		ApiResponseMessage responseMessage = new ApiResponseMessage();
		responseMessage.setMessage("Category is deleted successfully !!");
		responseMessage.setSuccess(true);
		responseMessage.setHttpStatus(HttpStatus.OK);
		return new ResponseEntity<>(responseMessage, HttpStatus.OK);

	}

	// get all

	@GetMapping(CATEGORY_GET_ALL)
	public ResponseEntity<PageableResponse<CategoryDto>> getAll(
			@RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
			@RequestParam(value = "sortBy", defaultValue = "title", required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir

	) {
		PageableResponse<CategoryDto> pageableResponse = categoryService.getAll(pageNumber, pageSize, sortBy, sortDir);
		return new ResponseEntity<>(pageableResponse, HttpStatus.OK);
	}

	// get single  CATEGORY_GET_ID
	@GetMapping(CATEGORY_GET_ID)
	public ResponseEntity<CategoryDto> getSingle(@PathVariable String categoryId) {
		CategoryDto categoryDto = categoryService.get(categoryId);
		return ResponseEntity.ok(categoryDto);
	}

	// create product with category
	@PostMapping(CATEGORY_PRODUCT_CREATE_BY_CID)
	public ResponseEntity<ProductDto> createProductWithCategory(@PathVariable("categoryId") String categoryId,
			@RequestBody ProductDto dto) {
		ProductDto productWithCategory = productService.createWithCategory(dto, categoryId);
		return new ResponseEntity<>(productWithCategory, HttpStatus.CREATED);
	}

	// update category of product
	@PutMapping(CATEGORY_PRODUCT_UPDATE_BY_AID)
	public ResponseEntity<ProductDto> updateCategoryOfProduct(@PathVariable String categoryId,
			@PathVariable String productId) {
		ProductDto productDto = productService.updateCategory(productId, categoryId);
		return new ResponseEntity<>(productDto, HttpStatus.OK);
	}

	// get products of categories
	@GetMapping(CATEGORY_GET_PRODUCT_BY_CID)
	public ResponseEntity<PageableResponse<ProductDto>> getProductsOfCategory(@PathVariable String categoryId,
			@RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
			@RequestParam(value = "sortBy", defaultValue = "title", required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir

	) {

		PageableResponse<ProductDto> response = productService.getAllOfCategory(categoryId, pageNumber, pageSize,
				sortBy, sortDir);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

}
