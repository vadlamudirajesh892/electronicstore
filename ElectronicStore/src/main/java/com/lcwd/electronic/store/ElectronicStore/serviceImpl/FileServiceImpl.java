package com.lcwd.electronic.store.ElectronicStore.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.lcwd.electronic.store.ElectronicStore.exception.BadApiResponseException;
import com.lcwd.electronic.store.ElectronicStore.service.FileService;

@Service
public class FileServiceImpl implements FileService {

	private Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

	public String uploadFile(MultipartFile file, String path)  {

		String originalFileName = file.getOriginalFilename();
		logger.info("Filename : :{} ", originalFileName);
		String filename = UUID.randomUUID().toString();
		String extension = originalFileName.substring(originalFileName.lastIndexOf("."));
		String fileNameWithExtension = filename + extension;
		String fullPathWithFileName = path + File.separator + fileNameWithExtension;

		logger.info("full image path :: {} ",fullPathWithFileName);
		if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")) {

			logger.info(" file extension is ::{}",extension);
			File folder = new File(path);
			if (!folder.exists()) {
				folder.mkdirs();
			}

			try {
				Files.copy(file.getInputStream(), Paths.get(fileNameWithExtension));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return fileNameWithExtension;
		} else {
			logger.info("Image path upload ENDED HERE @@@@@@@@ !!!");
			throw new BadApiResponseException("File with this " + extension + " not allowed !!");
		}
	}

	public InputStream getResource(String path, String name)   {

		String fullPath = path + File.separator + name;
		 InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(fullPath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return inputStream;
	}
}
