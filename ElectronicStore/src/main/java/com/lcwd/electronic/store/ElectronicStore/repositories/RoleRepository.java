package com.lcwd.electronic.store.ElectronicStore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lcwd.electronic.store.ElectronicStore.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
}
