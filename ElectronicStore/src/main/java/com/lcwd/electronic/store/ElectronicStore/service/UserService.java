package com.lcwd.electronic.store.ElectronicStore.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.dtos.UserDto;
import com.lcwd.electronic.store.ElectronicStore.entities.User;

@Service
public interface UserService {

	// create
	UserDto createUser(UserDto userDto);

	// update
	UserDto updateUser(UserDto userDto, String userId);

	// delete
	void deleteUser(String userid) throws IOException;

	// get all users
	/*
	 * public List<UserDto> getAllUser(int pageNumber,int pageSize,String
	 * sortBy,String sortDir);
	 */
	public PageableResponse<UserDto> getAllUser(int pageNumber, int pageSize, String sortBy, String sortDir);

	// get single user by id
	UserDto getUserById(String userId);

	UserDto getUserByEmail(String email);

	// serach user
	List<UserDto> searchUser(String keyword);

}
