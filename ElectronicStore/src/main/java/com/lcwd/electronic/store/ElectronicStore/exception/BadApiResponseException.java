package com.lcwd.electronic.store.ElectronicStore.exception;

public class BadApiResponseException extends RuntimeException {

	public BadApiResponseException() {
		super("Bad request");
	}

	public BadApiResponseException(String msg) {
		super(msg);
	}
}
