package com.lcwd.electronic.store.ElectronicStore.constants;

public class LoggerConstants {

	private LoggerConstants() {
		
	}
	//UserConstoller constants
	public static final String SAVE_USER="saveUsers_for_:: STARTED";
	public static final String SAVE_USER_END="saveusers _for :: END";
	public static final String FIND_USER="getUsers::";
	public static final String FIND_USER_ID_START="getUserById :: START";
	public static final String FIND_USER_ID_END="getUserById :: END";
	public static final String USER_IMAGE_NAME="User image with name :: ";
	
	//UserServiceImpl Constants
	public static final String SAVE_USERS="saveUsers :: ";
	public static final String SAVE_USERS_SUCCESS="users saving successfully!";
	public static final String SAVE_USERS_FAIL=" Failed to save users";
	public static final String ALL_USERS_START="getAllUsers :: START";
	public static final String ALL_USERS_END="getAllUsers :: END";
	public static final String SAVE_USERS_DETAILS_START="getAllUsers :: START";
	public static final String SAVE_USERS_DETAILS_END="getAllUsers :: START";
	public static final String SAVE_USERS_DETAILS="user Details saved in electronicstore";
	public static final String USER_IMAGE_NOT_FOUND="user Image not found in folder in given path ::";
	
	
	
}
