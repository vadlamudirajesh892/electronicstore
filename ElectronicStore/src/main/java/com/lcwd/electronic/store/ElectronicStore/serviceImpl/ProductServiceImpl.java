package com.lcwd.electronic.store.ElectronicStore.serviceImpl;

import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.ALL_PRODUCT_CATEGORY_EXCEPTION_RESOUCE_GET;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.PRODUCT_EXCEPTION_RESOURCE_CATEGORY_ID_CREATE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.PRODUCT_EXCEPTION_RESOURCE_DELETE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.PRODUCT_EXCEPTION_RESOURCE_GET;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.PRODUCT_EXCEPTION_RESOURCE_UPDATE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.UPDATE_CATEGORY_EXCEPTION_RESOUCE_GET;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.UPDATE_PRODUCT_EXCEPTION_RESOURCE_GET;

import java.util.Date;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.dtos.ProductDto;
import com.lcwd.electronic.store.ElectronicStore.entities.Category;
import com.lcwd.electronic.store.ElectronicStore.entities.Product;
import com.lcwd.electronic.store.ElectronicStore.exception.ResourceNotFoundExpeption;
import com.lcwd.electronic.store.ElectronicStore.helper.Helper;
import com.lcwd.electronic.store.ElectronicStore.repositories.CategoryRepository;
import com.lcwd.electronic.store.ElectronicStore.repositories.ProductRepository;
import com.lcwd.electronic.store.ElectronicStore.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ModelMapper mapper;

	@Autowired
	private CategoryRepository categoryRepository;

	// other dependency
	@Override
	public ProductDto create(ProductDto productDto) {

		Product product = mapper.map(productDto, Product.class);

		// product id
		String productId = UUID.randomUUID().toString();
		product.setProductId(productId);
		// added
		product.setAddedDate(new Date());
		Product saveProduct = productRepository.save(product);
		return mapper.map(saveProduct, ProductDto.class);
	}

	@Override
	public ProductDto update(ProductDto productDto, String productId) {

		// fetch the product of given id
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(PRODUCT_EXCEPTION_RESOURCE_UPDATE));
		product.setTitle(productDto.getTitle());
		product.setDescription(productDto.getDescription());
		product.setPrice(productDto.getPrice());
		product.setDiscountedPrice(productDto.getDiscountedPrice());
		product.setQuantity(productDto.getQuantity());
		product.setLive(productDto.isLive());
		product.setStock(productDto.isStock());
		product.setProductImageName(productDto.getProductImageName());

//        save the entity
		Product updatedProduct = productRepository.save(product);
		return mapper.map(updatedProduct, ProductDto.class);
	}

	@Override
	public void delete(String productId) {
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(PRODUCT_EXCEPTION_RESOURCE_DELETE));
		productRepository.delete(product);
	}

	@Override
	public ProductDto get(String productId) {
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(PRODUCT_EXCEPTION_RESOURCE_GET));
		return mapper.map(product, ProductDto.class);
	}

	@Override
	public PageableResponse<ProductDto> getAll(int pageNumber, int pageSize, String sortBy, String sortDir) {
		Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
		Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
		Page<Product> page = productRepository.findAll(pageable);
		return Helper.getPageableResponse(page, ProductDto.class);
	}

	@Override
	public PageableResponse<ProductDto> getAllLive(int pageNumber, int pageSize, String sortBy, String sortDir) {
		Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
		Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
		Page<Product> page = productRepository.findByLiveTrue(pageable);
		return Helper.getPageableResponse(page, ProductDto.class);
	}

	@Override
	public PageableResponse<ProductDto> searchByTitle(String subTitle, int pageNumber, int pageSize, String sortBy,
			String sortDir) {
		Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
		Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
		Page<Product> page = productRepository.findByTitleContaining(subTitle, pageable);
		return Helper.getPageableResponse(page, ProductDto.class);
	}

	@Override
	public ProductDto createWithCategory(ProductDto productDto, String categoryId) {
		// fetch the category from db:
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(PRODUCT_EXCEPTION_RESOURCE_CATEGORY_ID_CREATE));
		Product product = mapper.map(productDto, Product.class);

		// product id
		String productId = UUID.randomUUID().toString();
		product.setProductId(productId);
		// added
		product.setAddedDate(new Date());
		product.setCategory(category);
		Product saveProduct = productRepository.save(product);
		return mapper.map(saveProduct, ProductDto.class);

	}

	@Override
	public ProductDto updateCategory(String productId, String categoryId) {
		// product fetch
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(UPDATE_PRODUCT_EXCEPTION_RESOURCE_GET));
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(UPDATE_CATEGORY_EXCEPTION_RESOUCE_GET));
		product.setCategory(category);
		Product savedProduct = productRepository.save(product);
		return mapper.map(savedProduct, ProductDto.class);
	}

	@Override
	public PageableResponse<ProductDto> getAllOfCategory(String categoryId, int pageNumber, int pageSize, String sortBy,
			String sortDir) {
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundExpeption(ALL_PRODUCT_CATEGORY_EXCEPTION_RESOUCE_GET));
		Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
		Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
		Page<Product> page = productRepository.findByCategory(category, pageable);
		return Helper.getPageableResponse(page, ProductDto.class);
	}
}
