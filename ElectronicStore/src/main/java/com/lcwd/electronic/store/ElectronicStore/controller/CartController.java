package com.lcwd.electronic.store.ElectronicStore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lcwd.electronic.store.ElectronicStore.dtos.AddItemToCartRequest;
import com.lcwd.electronic.store.ElectronicStore.dtos.ApiResponseMessage;
import com.lcwd.electronic.store.ElectronicStore.dtos.CartDto;
import com.lcwd.electronic.store.ElectronicStore.helper.Helper;
import com.lcwd.electronic.store.ElectronicStore.service.CartService;

@RestController
@RequestMapping("/carts")
public class CartController {

	@Autowired
	private CartService cartService;

	ApiResponseMessage response = Helper.getInstance();

	// add items to cart
	@PostMapping("/{userId}")
	public ResponseEntity<CartDto> addItemToCart(@PathVariable String userId,
			@RequestBody AddItemToCartRequest request) {
		CartDto cartDto = cartService.addItemToCart(userId, request);
		return new ResponseEntity<>(cartDto, HttpStatus.OK);
	}

	@DeleteMapping("/{userId}/items/{itemId}")
	public ResponseEntity<ApiResponseMessage> removeItemFromCart(@PathVariable String userId,
			@PathVariable int itemId) {
		cartService.removeItemFromCart(userId, itemId);

		/*
		 * ApiResponseMessage response = ApiResponseMessage.builder()
		 * .message("Item is removed !!") .success(true) .status(HttpStatus.OK)
		 * .build();
		 */
		response.setMessage("Item is removed !!");
		response.setHttpStatus(HttpStatus.OK);
		response.setSuccess(true);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	// clear cart
	@DeleteMapping("/{userId}")
	public ResponseEntity<ApiResponseMessage> clearCart(@PathVariable String userId) {
		cartService.clearCart(userId);
		/*
		 * ApiResponseMessage response =
		 * ApiResponseMessage.builder().message("Now cart is blank !!").success(true)
		 * .status(HttpStatus.OK).build();
		 */
		
		response.setMessage("Now cart is blank !!");
		response.setHttpStatus(HttpStatus.OK);
		response.setSuccess(true);
	
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	// add items to cart
	@GetMapping("/{userId}")
	public ResponseEntity<CartDto> getCart(@PathVariable String userId) {
		CartDto cartDto = cartService.getCartByUser(userId);
		return new ResponseEntity<>(cartDto, HttpStatus.OK);
	}

}
