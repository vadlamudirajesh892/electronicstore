package com.lcwd.electronic.store.ElectronicStore.constants;

public final class ElectronicStoreConstants {

	private ElectronicStoreConstants() {
	}

	//1. UserConstoller Constants
	public static final String USER_CONSTOLLER_REQUEST_MAPPIGN = "api/user";
	public static final String SAVE="/create";
	public static final String FIND_ALL="/get/all";
	public static final String FIND_BY_ID="/get/userbyid";
	public static final String FIND_BY_IMAGENAME="getbyimagename";
	public static final String FIND_BY_EMAIL="/getbyemail";
	public static final String DELETE_BY_ID="/deletebyid";
	public static final String UPDATE_BY_ID="/updatebyid";
	
	//2. Category controller Constants
	public static final String CATEGORY_CONTROLLER_REQUEST_MAPPING="api/category";
	public static final String CATEGORY_SAVE="/create";
	public static final String CATEGORY_UPDATE="/update/{categoryId}";
	public static final String CATEGORY_DELETE="/delete/{categoryId}";
	public static final String CATEGORY_GET_ALL="/get/all";
	public static final String CATEGORY_GET_ID="/get/{categoryId}";
	public static final String CATEGORY_PRODUCT_CREATE_BY_CID="/{categoryId}/products";
	public static final String CATEGORY_PRODUCT_UPDATE_BY_AID="/{categoryId}/products/{productId}";
	public static final String CATEGORY_GET_PRODUCT_BY_CID="/{categoryId}/products";
	
	//3. Product constroller Constants
	public static final String PRODUCT_CONTROLLER_REQUEST_MAPPING="api/product";
	public static final String PRODUCT_SAVE="/create";
	public static final String PRODUCT_UPDATE="/update/{}";
	public static final String PRODUCT_DELETE="/delete/{}";
	public static final String PRODUCT_GET_ALL="/get/all";
	public static final String PRODUCT_GET_ID="/get/{}";
	//public static final String PRODUCT
	
	//4. Order constroller Constants
	public static final String ORDER_CONTROLLER_REQUEST_MAPPING="/api/order";
	public static final String ORDER_BY_UID="/users/{userId}"; 
	public static final String ORDER_ALL="/all";
	public static final String ORDER_SAVE="/create";
	public static final String ORDER_DELETE_BY_OID="/delete/{orderId}";
	
	//5.Cart controller Constants 
	//Category serviceImpl Constants
	public static final String CATEGORY_EXCEPTION_RESOURCE_UPDATE="Category not found with given(UPDATE) id :: [ServiceImpl]";
	public static final String CAtegory_EXCEPTION_RESOURCE_DELETE="Category not found with given(DELETE) id :: [ServiceImpl]";
	public static final String UPDATE_PRODUCT_EXCEPTION_RESOURCE_GET="Category not found with given(GET) id :: [ServiceImpl]";
	public static final String ALL_PRODUCT_CATEGORY_EXCEPTION_RESOUCE_GET="category not Found !![ServiceImpl]";
	public static final String ID_CATEGORY_EXCEPTION_RESOUCE_GET="category not Found with given(GETBY) Id ::[ServiceImpl]";
	//Product ServiceImpl constants
	public static final String PRODUCT_EXCEPTION_RESOURCE_UPDATE="Product not found of given(UPDATE) Id :: [productServiceImpl]";
	public static final String PRODUCT_EXCEPTION_RESOURCE_DELETE="Product not found of given(DELETE) Id :: [productServiceImpl]";
	public static final String PRODUCT_EXCEPTION_RESOURCE_GET="Product not found of given(GET) Id !![productServiceImpl]";
	public static final String PRODUCT_EXCEPTION_RESOURCE_CATEGORY_ID_CREATE="Category not found GIVEN CID :: [productServiceImpl]";
	public static final String UPDATE_CATEGORY_EXCEPTION_RESOUCE_GET="Product not found of given(GET) Id :: [productServiceImpl]";
	
	//User serviceImpl constants
	public static final String USER_EXCEPTION_RUNTIME_UPDATE="user not found with given(UPDATE) Id::[userServiceImpl]";
	public static final String USER_EXCEPTION_RUNTIME_DELETE="user not found with given(DELETE) Id::[userServiceImpl]";
	public static final String USER_EXCEPTION_RESOURCE_GET="user not found with given(GET) Id:: [UserServiceImpl]";
	public static final String USER_EXCEPTION_RESOURCE_GET_EMAIL="user not found with given(GET) emailId:: [UserServiceImpl]";
	
	//User ServiceImpl
	public static final String SUCCESS="success";
	public static final String EVENT_TYPE_NEW="new";
	public static final String EVENT_TYPE_UPDATE="upadate";
	public static final String EVENT_TYPE_PROCUDE="proceduing";
	public static final String EVENT_TYPE_CANCEL="cancel";
	
	//OrderServiceImpl Constants
	public static final String ORDER_EXCEPRION_RESOURCE_CREATE_FIND_UID="User not found with given(POST) id :: [OrderServiceImpl]";
	public static final String ORDER_EXCEPTION_RESOURCE_CREATE_FIND_CARTID="User not found with given(POST) id :: [OrderServiceImpl]";
	public static final String ORDER_EXCEPTION_BEDAPI_RESPONSE_CREATE_CART_SIZE="Invalid number of items in cart !! [OrderServiceImpl]";
	public static final String ORDER_EXCEPTION_RESOURCE_DELETE_FIND_UID="order is not found ::";
}

