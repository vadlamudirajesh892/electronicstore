package com.lcwd.electronic.store.ElectronicStore.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.lcwd.electronic.store.ElectronicStore.dtos.ApiResponseMessage;

@RestControllerAdvice
public class GlobalExceptionHandler {

	private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(ResourceNotFoundExpeption.class)
	public ResponseEntity<ApiResponseMessage> resourceNotFoundException(ResourceNotFoundExpeption ex) {
		logger.info("Exception handler method invoked !!");

		ApiResponseMessage response = new ApiResponseMessage();
		response.setMessage(ex.getMessage());
		response.setHttpStatus(HttpStatus.NOT_FOUND);
		response.setSuccess(true);
		return new ResponseEntity<ApiResponseMessage>(response, HttpStatus.NOT_FOUND);
	}

	// MethodArgumentNotValidException
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, Object>> handleMethodArgumentNotvalidException(
			MethodArgumentNotValidException ex) {
		List<ObjectError> allErrors = ex.getBindingResult().getAllErrors();
		Map<String, Object> response = new HashMap<String, Object>();
		allErrors.stream().forEach(ObjectError -> {
			String defaultMessage = ObjectError.getDefaultMessage();
			String field = ((FieldError) ObjectError).getField();
			response.put(defaultMessage, field);
		});
		return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(BadApiResponseException.class)
	public ResponseEntity<ApiResponseMessage> BadApiResponseException(BadApiResponseException ex) {
		logger.info("Bad Api response !!");

		ApiResponseMessage response = new ApiResponseMessage();
		response.setMessage(ex.getMessage());
		response.setHttpStatus(HttpStatus.BAD_REQUEST);
		response.setSuccess(false);
		return new ResponseEntity<ApiResponseMessage>(response, HttpStatus.BAD_REQUEST);
	}
}
