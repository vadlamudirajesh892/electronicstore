package com.lcwd.electronic.store.ElectronicStore.helper;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;

import com.lcwd.electronic.store.ElectronicStore.dtos.ApiResponseMessage;
import com.lcwd.electronic.store.ElectronicStore.dtos.CreateOrderRequest;
import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.entities.Order;

public class Helper {
//U=Entity,V=Dto
	private static final ApiResponseMessage response = null;

	public static <U, V> PageableResponse<V> getPageableResponse(Page<U> page, Class<V> type) {

		List<U> entity = page.getContent();
		List<V> dtoList = entity.stream().map(object -> new ModelMapper().map(object, type))
				.collect(Collectors.toList());

		PageableResponse<V> response = new PageableResponse();
		response.setContent(dtoList);
		response.setPageNumber(page.getNumber());
		response.setPageSize(page.getSize());
		response.setTotalElements(page.getTotalElements());
		response.setTotalPages(page.getTotalPages());
		response.setLastPage(page.isLast());
		return response;
	}

	public static ApiResponseMessage getInstance() {
		if (response == null) {
			return new ApiResponseMessage();
		} else {
			return response;
		}
	}
	
	public  static Order dtoToEntity(CreateOrderRequest createOrderRequest) {
		Order order=new Order();
		   Order order2 = new Order();
		order.setBillingName(createOrderRequest.getBillingName());
	        order.setBillingPhone(createOrderRequest.getBillingPhone());
	        order.setBillingAddress(createOrderRequest.getBillingAddress());
	        order.setOrderedDate(new Date());
	        order.setDeliveredDate(null);
	        order.setPaymentStatus(createOrderRequest.getPaymentStatus());
	        order.setOrderStatus(createOrderRequest.getOrderStatus());
	        order.setOrderId(UUID.randomUUID().toString());
			return order;
	}
}
