package com.lcwd.electronic.store.ElectronicStore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lcwd.electronic.store.ElectronicStore.entities.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Integer>
{
}
