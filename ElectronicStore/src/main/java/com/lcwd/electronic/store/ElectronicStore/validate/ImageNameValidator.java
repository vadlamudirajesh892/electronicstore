package com.lcwd.electronic.store.ElectronicStore.validate;

import javax.validation.ConstraintValidator;

import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageNameValidator implements ConstraintValidator<ImageNameValid, String> {

	private Logger logger = LoggerFactory.getLogger(ImageNameValid.class);

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// logic
		logger.info(" Message from isValid : {} ", value);
		if (value.isBlank()) {
			return false;
		}
		return true;
	}

}
