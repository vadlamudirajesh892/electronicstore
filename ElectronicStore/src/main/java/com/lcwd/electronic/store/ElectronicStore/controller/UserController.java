package com.lcwd.electronic.store.ElectronicStore.controller;

import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.SAVE;
import static com.lcwd.electronic.store.ElectronicStore.constants.ElectronicStoreConstants.USER_CONSTOLLER_REQUEST_MAPPIGN;
import static com.lcwd.electronic.store.ElectronicStore.constants.LoggerConstants.USER_IMAGE_NAME;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lcwd.electronic.store.ElectronicStore.dtos.ApiResponseMessage;
import com.lcwd.electronic.store.ElectronicStore.dtos.ImageResponse;
import com.lcwd.electronic.store.ElectronicStore.dtos.PageableResponse;
import com.lcwd.electronic.store.ElectronicStore.dtos.UserDto;
import com.lcwd.electronic.store.ElectronicStore.service.FileService;
import com.lcwd.electronic.store.ElectronicStore.service.UserService;

import lombok.extern.slf4j.Slf4j;

/*
This is common controller
*/
@RestController
@RequestMapping(USER_CONSTOLLER_REQUEST_MAPPIGN)
@Slf4j
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private FileService fileService;

	@Value("${user.profile.image.path}")
	private String imageUploadpath;

	private Logger log = LoggerFactory.getLogger(UserController.class);

	// 1.CREATE
	@PostMapping(path = SAVE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDto> createUser(@Valid @RequestBody() UserDto userDto) {
		// log.info("dsggf");
		UserDto userDto1 = userService.createUser(userDto);
		return new ResponseEntity(userDto1, HttpStatus.CREATED);
	}

	// 2.UPDATE
	@PutMapping("/update/{userId}")
	public ResponseEntity<UserDto> updateUser(@PathVariable("userId") String userId,
			@Valid @RequestBody UserDto userDto) {
		UserDto updateUserDto = userService.updateUser(userDto, userId);
		return new ResponseEntity(updateUserDto, HttpStatus.OK);
	}

	// 3. DELETE
	@DeleteMapping("/delete/{userId}")
	public ResponseEntity<ApiResponseMessage> deleteUser(@PathVariable String userId) throws IOException {
		userService.deleteUser(userId);
		// HERE ApiResponseMessage IS CONVERTED IN TO JSON FORMAT
		ApiResponseMessage api = new ApiResponseMessage();
		api.setMessage("user deleted successfully !!");
		api.setSuccess(true);
		api.setHttpStatus(HttpStatus.OK);
		return new ResponseEntity(api, HttpStatus.OK);
	}

	// GET ALL
	// public ResponseEntity<List<UserDto>> getAllUsers(
	@GetMapping("/all")
	public ResponseEntity<PageableResponse<UserDto>> getAllUsers(
			@RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
			@RequestParam(value = "sortBy", defaultValue = "name", required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir) {

		return new ResponseEntity<>(userService.getAllUser(pageNumber, pageSize, sortBy, sortDir), HttpStatus.OK);
	}

	// 4.GET SINGLE
	@GetMapping("/id/{uId}")
	public ResponseEntity<UserDto> getUser(@PathVariable("uId") String userId) {
		return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.OK);
	}

	// 5.GET BY EMAIL
	@GetMapping("/email/{emailId}")
	public ResponseEntity<UserDto> getUserByEmail(@PathVariable("emailId") String email) {
		return new ResponseEntity<>(userService.getUserByEmail(email), HttpStatus.OK);
	}

	// 6.SEARCH USER
	@GetMapping("/search/{keywords}")
	public ResponseEntity<List<UserDto>> searchUser(@PathVariable String keywords) {
		return new ResponseEntity<>(userService.searchUser(keywords), HttpStatus.OK);
	}

	// Upload user Image
	@PostMapping("/image/{userId}")
	public ResponseEntity<ImageResponse> uploadUserImage(@RequestParam(name = "userImage" ,required = false) MultipartFile image,
			@PathVariable String userId) throws IOException {
		String imageName = fileService.uploadFile(image, userId);

		UserDto user = userService.getUserById(userId);
		user.setImageName(imageName);
		UserDto userDto = userService.updateUser(user, userId);

		ImageResponse response = new ImageResponse();
		response.setImageName(imageName);
		response.setSuccess(true);
		response.setStatus(HttpStatus.CREATED);
		return new ResponseEntity(response, HttpStatus.CREATED);
	}
	// Serve User Image

	@GetMapping("/image/{userId}")
	public void serveUserImage(@PathVariable String userId,HttpServletResponse response) throws IOException {
		UserDto userDto = userService.getUserById(userId);
		log.info(USER_IMAGE_NAME, userDto.getName());
		InputStream resource = fileService.getResource(imageUploadpath, userDto.getImageName());
		response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		StreamUtils.copy(resource,response.getOutputStream());
		
	}
	// enable validation use @Valid used for post and putRequest at method argument

}
