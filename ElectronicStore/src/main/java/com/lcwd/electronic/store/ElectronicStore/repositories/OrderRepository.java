package com.lcwd.electronic.store.ElectronicStore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lcwd.electronic.store.ElectronicStore.entities.Order;
import com.lcwd.electronic.store.ElectronicStore.entities.User;

public interface OrderRepository extends JpaRepository<Order, String> {

    List<Order> findByUser(User user);

}
