package com.lcwd.electronic.store.ElectronicStore.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.lcwd.electronic.store.ElectronicStore.security.JwtAuthenticationEntryPoint;
import com.lcwd.electronic.store.ElectronicStore.security.JwtAuthenticationFilter;

@Configuration
public class SecurityConfig {

	@Autowired
	private UserDetailsService userDatialsService;

	@Autowired
	private JwtAuthenticationEntryPoint authenticationEntryPoint;
	@Autowired
	private JwtAuthenticationFilter authenticationFilter;

	// 3.java based Authontication

	/*
	 * @Bean public UserDetailsService userDetailssService() {
	 * 
	 * 
	 * Collection<? extends GrantedAuthority> l=new ArrayList<>();
	 * org.springframework.security.core.userdetails.User u=new
	 * User("Rajesh","9963",l);
	 * 
	 * // InMemoryUserDetailsManager is the implementation class of
	 * UserDetailService
	 * 
	 * UserDetails normal =
	 * User.builder().username("Ramu").password(passwordEncoder().encode("Ramu")).
	 * roles("Normal") .build(); UserDetails admin =
	 * User.builder().username("Kevin").password(passwordEncoder().encode("Kevin")).
	 * roles("Admin") .build();
	 * 
	 * return new InMemoryUserDetailsManager(normal, admin);
	 * 
	 * }
	 */

	// data based config start

	/*
	 * @Bean public SecurityFilterChain securityFilterChain(HttpSecurity http)
	 * throws Exception {
	 * 
	 * // form based authentication for browser
	 * 
	 * 
	 * http.authorizeRequests().anyRequest().authenticated().and().formLogin().
	 * loginPage("login.html")
	 * .loginProcessingUrl("/process-url").defaultSuccessUrl("/dashboard").
	 * failureUrl("/error").and().logout() .logoutUrl("/do-logout"); return null;
	 * 
	 * 
	 * basic authentication for front end
	 * 
	 * 
	 * http.csrf().disable().authorizeRequests().antMatchers("/auth/login").
	 * permitAll().antMatchers("/auth/google")
	 * .permitAll().antMatchers(HttpMethod.POST, "/users").permitAll()
	 * .antMatchers(HttpMethod.DELETE, "/users/**")
	 * .hasRole("ADMIN").antMatchers(PUBLIC_URLS).permitAll()
	 * .antMatchers(HttpMethod.GET).permitAll().anyRequest()
	 * .authenticated().and().exceptionHandling()
	 * .authenticationEntryPoint(authenticationEntryPoint)
	 * .and().sessionManagement()
	 * .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	 * http.addFilterBefore(authenticationFilter,
	 * UsernamePasswordAuthenticationFilter.class); return http.build();
	 * 
	 * return null; }
	 */

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(this.userDatialsService);
		provider.setPasswordEncoder(passwordEncoder());
		return provider;
	}

	// data db based config end_:user, customserviceimpl;
	@Bean
	public PasswordEncoder passwordEncoder() {

		return new BCryptPasswordEncoder();
	}

	// 4.Data base authentication

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration builder) throws Exception {
		return builder.getAuthenticationManager();
	}

}
